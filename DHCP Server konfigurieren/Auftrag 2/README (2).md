
# Auftrag 2

Für welches Subnetz ist der DHCP Server aktiv?
Der DHCP Server ist fuür diese Subnetzmaske aktiviert 192.168.26.1 - 192.168.26.254.

Welche IP-Adressen ist vergeben und an welche MAC-Adressen?

IP-Addresse|MAC-Adresse
-----------|-----------
192.168.26.26|0007.ECB6.4534
192.168.26.25|00E0.8F4E.65AA
192.168.26.24|0050.0F4E.1D82
192.168.26.22|0009.7CA6.4CE3
192.168.26.23|0001.632C.3508
192.168.26.27|00D0.BC52.B29B

In welchem Range vergibt der DHCP-Server IPv4 Adressen?
In dieser Range vergibt der DHCP-Server die IP-Adresse 192.168.26.21-192.168.26.190.

Was hat die Konfiguration ip dhcp excluded-address zur Folge?
Das die IP-Adressen von 192.168.26.1-192.168.26.21 und 192.168.26.190-192.168.26.238 Fixe IP-Adressen sind, die von Hand vergeben werden.

Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?
254 IP-Adressen können vergeben werden.

Welcher OP-Code hat der DHCP-Offer?
Op Code (op) = 2 (0x2)

Welcher OP-Code hat der DHCP-Request?
Op Code (op) = 1 (0x1)

Welcher OP-Code hat der DHCP-Acknowledge?
Op Code (op) = 2 (0x2)

An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?
Es wird an alle geschickt damit ermittelt wird welcher der Server ist. 

An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?
Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?
Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?
Welche IPv4-Adresse wird dem Client zugewiesen?
