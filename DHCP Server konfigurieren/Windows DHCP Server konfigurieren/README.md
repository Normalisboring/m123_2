
<h1><font>DHCP-Server Konfigurieren</font></h1>

---

[TOC]

## Einleitung

In dieser Dokumentation wird erklärt, wie man auf Filius einen **DHCP-Server** aufsetzt und konfiguriert.

## Struktur

Bei der Ausgangslage handelt es sich um, eine **Stern-Topologie** mit einem **DHCP-Server** und **drei Clients**, die durch ein Kabel verbunden werden.

![Architektur](./images/Start%20Bild.png)

## Clients Aufsetzten

|Client   |IP-Adresse | Mac-Adresse|
|---------|-----------|------------|
|Client 1 |192.168.0.150-192.168.0.170|
|Client 2 |192.168.0.150-192.168.0.170|
|Client 3 |192.168.0.50 (Statisch)|49:11:5C:DA:77:23|

Die Mac-Adresse, ist eine Beispiel Adresse, um zu zeigen wie die folgenden Schritte gehen.

Bei allen Clients muss der Hacken **DHCP zur Konfiguration verwenden aktiviert** werden.

![Architektur](./images/DHCP%20Kofig.png)

## DHCP-Server aufsetzen

Beim DHCP-Server muss einwenig mehr gemacht werde. Ihr müsst den DHCP eine IP-Adresse geben, die zum Beispiel 192.168.0.10 lautet.

|Server|IP-Adresse |
|------|-----------|
|DHCP  |192.168.0.10|

![Architektur](./images/IP-Adresse%20DHCP.png)

Im Anschluss wird der DHCP durch diese zwei Klicks noch Aktiviert.

1. ![Architektur](./images/2.png)   2. ![Architektur](./images/3.png)

## Spezialfall (Client mit Statischer IP-Adresse)

Dann ist hier noch unser Spezialfall den wird ändern wollen. Kopiert die MAC-Adresse von Client 3 (Sieht bei jedem anders aus).

![Architektur](./images/Mac-Adresse%20Spezialfall.png)

Gehe zum DHCP-Server klicke wieder auf DHCP-Server einrichten, dann auf Statische Adresszuweisung, gebe hier die MAC-Adresse ein und im underen Feld die gewünschte IP_Adresse. Zum Schluss klickst du noch auf hinzufügen und dan OK.

![Architektur](./images/DHCP%20Server.png)

<h1><center>Ergebnis 🥳</center></h1>

![Architektur](./images/Zeigt%20das%20es%20funktioniert.png)

>Created by Vanessa Schellenbaum
