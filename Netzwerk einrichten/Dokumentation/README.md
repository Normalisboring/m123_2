<h1><font>Netzwerk einrichten von Splendid Blur AG</font></h1>

---

[TOC]

## Einleitung

Ich werde euch hier zeigen wie man das Netzwerk von **Splendid Blur AG** auf Fillius einrichtet.

## Struktur

Bei der Ausgangslage handelt es sich um, das Netzwerk von der Firma **Spledid Blur AG**.

![Bild](../images/Struktur%20.png)

## Erste Schritte

Als erstes müssen wir alles richtig benennen und Verbinden. Dann den Statischen Clients die richtige IP-Adresse geben, sowie den richtigen Gateway. Bei dem Server genau das selbe.

## DHCP-Server aufsetzen

Klicken sie bei DHCP Server auf DHCP Server einrichten.

![Bild](../images/DHCP%20Server%20einrichten.png)

Beim DHCP-Server müssen die Cients hinzugefügt werden, wie unten Abgebildet. Die Adress-Untergrenze und Obergrenze muss auch angegeben werden.

1. ![Architektur](../images/Adresslimiten%20für%20DHCP.png)
2. ![Bild](../images/PCs%20dem%20DHCP%20zugewiesen.png)

Im Anschluss wird der DHCP Server durch diesen Klick aktiviert.

![Bild](../images/DHCP%20aktivieren.png)

## Der Verbindungsrechner richtig einstellen

Geben sie hier die richtigen IP-Adressen und Netzwerkmasken ein.

![Bild](../images/Switches%20IP%20vergeben.png)

## DNS Server Einstellungen

Schauen sie das es bei Ihnen genau so aussieht. Klicken sie am Schluss auf Starten.

![Bild](../images/DNS%20Server.png)

## Test und Fehlermeldungen

Wenn es bei Ihnen auch so aussieht funktioniert, dann haben sie alles richtig gemacht.🥳

![Bild](../images/Es%20funktioniert.jpg)

### Mögliche Fehlermeldung

Hier sehen sie das der Client nicht Pingen konnte. Das hat daran gelegen das ich falsche Gateways vergeben habe, also immer gut darauf achten das alles richtig eingegeben wurde.

![Bild](../images/Fehler%20meldung.png)