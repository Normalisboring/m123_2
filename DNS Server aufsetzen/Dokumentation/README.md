<h1><font>DNS-Server aufsetzen</font></h1>

---

[TOC]

## Einleitung

Hier erwartet sie eine Anleitung für das aufsetzen von einem DNS Server mit dem Betriebssystem von Windows mit einer Lesedauer von ca. 5 Minuten.

Ich starte mit der Annahme, dass sie schon eine Server von Windows installiert haben und jetzt bereit sind mit mir den Server, auf VMware Workstation Pro oder einem anderen Programm aufzusetzen. Ich habe mich für einen Windows Server 2022 entschieden.

## Voreinstellungen

Ihr Server sollte diese Voreinstellungen haben. Danach kann der Server auch schon gestartet werden.

![Bild](../images/1.%20Voreinstellungen.png)

## Statische IP-Adresse vergeben

Der erste Schritt besteht darin dem Server eine Statische IP-Adressse zu geben. Da sie gerade versuchen eine Server aufzusetzen, glaube ich daran das sie wissen wie eine Statische IP-Adresse vergeben wird und werde dies daher nicht erklären.

Am Schluss sollte es so aussehen muss aber nicht die gleiche IP-Adresse sein.

Der Grund wieso ich diese IP-Adressen gewählt habe, ist das sie die meistverwendeten sind. Es sind nähmlich die IP-Adressen von Localhost und Google.

![Bild](../images/Statische%20IP-Adresse.png)

## DNS Server auf Windows installieren

Schliessen sie nicht, das Fenster, das sich Automatische öffnet und den Namen Server Manger trägt.

|Bilder|Schritte|
|---|---|
|![Bild](../images/Server%20Manger.png)|Klicken sie auf Rollen und Features hinzufügen|
|![Bild](../images/Erster%20Schritt.png)|Klicken sie auf Installationstyp und wählen sie "Rollenbasierte oder featurebasierte Installation" aus.|
|![Bild](../images/Zweiter%20Schritt.png)|Klicken sie Ihren Server an. *Information: Falls ihr Server eine APIPA Adresse hat müssen sie ihre Netzwerkeinstellungen überprüfen und den Server neustarten.*|
|![Bild](../images/Dritter%20Schritt.png)|Installieren sie den DNS-Server|
|![Bild](../images/Vierter%20Schritt.png)|Hier können sie einfach auf Add Features klicken. (Lassen sie sich bitte nicht von der Sprachveränderung verwirren)
|![Bild](../images/Fünfte%20Schritt.png)|Die Features könne sie Ignorrieren und einfach auf Bestätigen klicken und **die Installation starten**.|
|![Bild](../images/Sechster%20Schritt.png)|Wenn es bei Ihnen auch so aussieht können sie das Fenster schliessen.

## Die Konfiguration

**Kleine Info am Rande:** Wenn sie die diese Konfiguration nicht machen ist der DNS Server nicht sichtbar im Netz.

### Forward-Lookupzonen Einrichtern

|Bilder|Schritte|
|---|---|
|![Bild](../images/Tool%20hinzufügen.png)|Öffnen sie die DNS Konfiguratione
|![Bild](../images/Neue%20Zone%20erstellen.png)|Rechtsklick auf "Forward-Lookupzonen" und "Neue Zone" anklicken.
|![BIld](../images/Willkommen.png)|Einfach weiter klicken.
|![Bild](../images/Zonen%20Typ.png)|Wählen sie die Primäre Zone aus.
|![BIld](../images/Zonen%20Name.png)|geben sie den gewünschten Namen ein.
|![Bild](../images/Zonen%20Dokument.png)|Erstellen sie ein neues Dokument.
|![Bild](../images/Dynamisches%20Update.png)|Wählen sie Dynamisches Update nicht zulassen.
|![Bild](../images/Fertigstellung.png)|Daten überprüfen und Fertig stellen klicken.

### Reverse-Lookupzonen Einrichten

|Bilder|Schritte|
|---|---|
|![Bild](../images/Neue%20Zone.png)|Rechtsklick auf "Reverse-Lookupzonen" und "Neue Zone" anklicken.
|![BIld](../images/Willkommen.png)|Einfach weiter klicken.
|![Bild](../images/Zonen%20Typ.png)|Wählen sie wieder die Primäre Zone aus.
|![Bild](../images/Erstellen%20von%20IPv4%20Lookup.png)| Hier erstellen wird eine IPv4 Reverse-Lookupzone, da wir IPv6 nicht haben.
|![Bild](../images/Netzwerk%20ID.png)|Geben sie Ihre Netzwerk-ID ein.
|![BIld](../images/Zonendatei.png)|Übernehmen Sie de Standard Einstellungen.
|![Bild](../images/Dynamisches%20Update.png)|Nehmen sie auch hier wieder die Auswahl Dynamische Updates nicht zulassen.
|![Bild](../images/Fertigstellen%202.png)|Bitte Kontrolieren und auf Fertig stellen drücken.

## Schon fast fertig...

---

## Host und Mail Exchanger hinzufügen

### Host hinzufügen

Jetzt müssen wir nur noch einen Host und einen Mail Exchanger hinzufügen sonst sieht den DNS Server nicht und das wäre ja schade.

|Bilder|Schritte|
|---|---|
|![Bild](../images/Neuer%20Host.png)|Klicken sie mit Rechtsklick auf ihre Forward-Lookupzonen die sie erstellt haben und wählen sie "Neuer Host" aus.
|![Bild](../images/Neuer%20Name%20und%20IP.png)|Hier geben sie bitte den gewünschten Namen und die IP-Adresse ein und setzen einen Hacken bei PTR-Eintrag. **Machen sie für den Client das selbe.**

### Mail Exchanger hinzufügen

Ich erstelle jetzt einen Mail Exchanger um zu testen ob auch einen Mail Exchanger auf dem Server funktioniert.

|Bilder|Schritte|
|---|---|
|![Bild](../images/Neuer%20Mail%20Excangerpng.png)|Klicken sie wieder mit Rechtsklick auf ihre Forward-Lookupzonen die sie erstellt haben aber wählen sie "Neuer Mail Exchanger" aus.
|![Bild](../images/Mail%20Daten.png)|Geben sie in ihre Hostdomain z.B. "mail" ein und wählen sie ihren Host den sie erstellt haben als FQDN aus.

### Weiterleitung für den DNS Server

|Bilder|Schritte|
|---|---|
|![Bild](../images/Eigenschaften%20für%208.8.8.8.png)|Damit der DNS Server auch Internet hat müssen wir die IP-Adresse 8.8.8.8 hinzufügen, die ist nähmlich für Google. Dafür Klicken wir mit Rechtsklick auf den DNS Server und gehen dann auf Eigenschaften.|
|![Bild](../images/Weiterleitung.png)|Dann gehen sie auf Weiterleitungen und klicken auf bearbeitung.|
|![Bild](../images/8.8.8.8%20hinzufügen.png)|Geben sie hier 8.8.8.8 ein und klicken sie auf OK. Zusätzluich müssen sie auf Vollqualifizierter Domänname klicken. Am Schluss noch auf Übernehmen klicken.

### NAT Anschluss für DNS Server

Wenn wir unserem DNS-Server eine zweiten NAT Anschluss geben. Dann kann er auch mit der ausen Welt komuniezieren.

|Bilder|Schritte|
|---|---|
|![Bild](../images/Zweiter%20NAT%20Anschluss.png)| Geben sie Ihrem DNS einen zweiten NAT Anschluss.
|![Bild](../images/Internet%20überprüfung.png)| Hier können sie überprüfen ob Ihr DNS Server eine NAT verbindung hat.

## Jetzt wird getestet

Jetzt sind wir Fertig aber funktioniert auch alles?
Schauen wir doch nach.

Als erstes muss CMD/Eingabeaufforderung geöffnet werden und Folgende commands eingegeben werden:

* `nslookup [Ihr Host] localhost`
* `nslookup [Ihr Mailexchanger] localhost`

Achten sie darauf das sie es korrekt eingeben, sonst passiert das.

![Bild](../images/Powershell.png)

## Test mit einem Client

Starten sie Ihren Client auf, lassen sie aber in der Zwischenzeit Ihren DNS Server angeschalten. Der Client sollte zwei Netzwerkadapter haben und einer davon sollte das M177 Host-Only Lab sein.

## DNS Server bei Client einrichten

Da Ihr Client zwei Netzwerkadapter hat, müssen wir zwei verschiedene Einstellungen machen. Die folgenden Einstellungen sollten bei beiden nicht mehr 127.0.0.1, sonder ihre eigenen Address Einstellungen für den Preferred DNS Server stehen. Bei mir ist das 192.168.100.160 und 8.8.8.8 ist für den Google Server.

![Bild](../images/Netzwerkadapter%20client%201.png)

Für den M117 Host-Only Netzwerkadapter sind das die folgenden Einstellungen:

![Bild](../images/Netzwerkadapter%20client%20.png)

## DNS Testen

Zu letzt Testen wir noch die Verbindung vom DNS zum Client.
Dies machen wir wieder in CMD/Eingabeaufforderung, geben sie hierfür diese commands ein:

* `nslookup [Ihr Host]`
* `nslookup [Ihr Mailexchanger]`

Wenn das dass Ergebnis ist dan haben sie alles richtig gemacht.🥳

![Bild](../images/Powershell%20von%20Client.png)

>Created by Vanessa Schellenbaum
