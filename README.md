# M123 Portfolio

[TOC]

Hier sehen sie ein Übersicht über meine Arbeit im Modul 123.

* [DHCP](https://gitlab.com/Normalisboring/m123_2/-/tree/main/DHCP%20Server%20konfigurieren?ref_type=heads)
* [DNS](https://gitlab.com/Normalisboring/m123_2/-/tree/main/DNS%20Server%20aufsetzen/Dokumentation?ref_type=heads)
* [Netzwerk für KMU](https://gitlab.com/Normalisboring/m123_2/-/tree/main/Netzwerk%20einrichten/Dokumentation?ref_type=heads)
* [Print-Server](https://gitlab.com/Normalisboring/m123_2/-/tree/main/Print%20Server%20aufsetzten/Dokumentation?ref_type=heads)
* [Samba](https://gitlab.com/Normalisboring/m123_2/-/blob/main/Samba%20Server/README.md?ref_type=heads)