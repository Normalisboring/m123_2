<h1><font>Samba Server aufsetzen</font></h1>

[TOC]

## Einleitung

Hier erwartet sie eine Anleitung für das aufsetzen von einem Samba Server mit dem Betriebssystem von Ubuntu, mit einer Lesedauer von ca. 3 Minuten.

Ich starte mit der Annahme, dass sie schon eine Server von Ubuntu installiert haben und jetzt bereit sind mit mir den Server, auf VMware Workstation Pro oder einem anderen Programm aufzusetzen. Ich habe mich für einen Ubuntu Server entschieden.

## Voreinstellungen

Ihr Server sollte diese Voreinstellungen haben.

![Bild](../images/Voreinstellungen.png)

Danach kann der Server auch schon gestartet werden.

## Statische IP-Adresse vergeben

Der erste Schritt besteht darin dem Server eine Statische IP-Adressse zu geben. Da sie gerade versuchen eine Server aufzusetzen, glaube ich daran das sie wissen wie eine Statische IP-Adresse vergeben wird und werde dies daher nicht erklären.

## Erster Schritt

Der erste Schritt besteht darin dieses Comando einzugeben.

> sudo apt upgrade

Dann kommt noch diese Comando beide dienen dazu den Samba auf dem Server zu Installieren.

> sudo apt install samba

Wenn du Testen willst ob alles erfolgreich geklappt hat dan gib folgendes ein:

> systemctl status smbd

![Bild](../images/Installalation%20abgeschlossen.png)

# Samba Konfigurieren

Diese Comando dient dazu das alles Automatisch startet.

> sudo systemctl enable --now smbd

Als nächstes geben wir dies Comando ein damît die Firewall dem Samba Server erlaubt die änderungen zu machen, die er möchte.

> sudo ufw allow samba

## Kreieren wir einen Ordner

Mit dem folgenden Befehl kreieren wir einen neuen Ordner.

> mkdir /home/[your username]/fileshare

Dieser Ordner bearbeiten wir jetzt dazu müssen wir zuerst in den Ordner mit diesem Comando.

> sudo nano /etc/samba/smb.conf

### Networking

Geben schauen sie das unter Networking dieses Comand steht:

> interfaces: 127.0.0.0/8 ens34

![Bild](../images/1%20networking.png) 

### Share Definitions

Unter Share Definitions sollte fogendes geändert werden.

>browseable = yes
read only = no

![Bild](../images/2%20Share%20Definitions.png)

### Ganz am Schluss

Zuunterst vom Dokument sollte folgendes eingegeben werden.

>[fileshare]
>comment = this is the config of our fileshare folder
>read only = yes
browseable = yes
guest ok = no

![Bild](../images/Sambashare.png)

## Einen Gast herstellen

Damit der Samba Server und der Client sich erreichen können brauchen wir eine neues Passwort.

> sudo smbpasswd -a [username]

![Bild](../images/neues%20passwort.png)

## Auf dem Windows Client Testen

Melden sie sich an und öffnen sie den Explorer. Da gibst du bitte folgendes ein:

> \\[Die IP-Adresse deines Servers]

![Bild](../images/Sambashare%20auf%20windowa.png)

Wenn alles richtig ist wird ein Ordner Namens Sambashare sichtbar.

Wenn du dan auf den Ordner klickst wird die Aufforderung kommen das du deine Log-in daten eingeben sollst. Wenn du dann auch noch in den Ordner kommst dann hat alles geklappt.

# Fehlermeldungen

Es kann sein das beim ersten mal so eine Fehler meldung auftaucht. Dann ist es wichtig Ruhe zu bewahren und nachzuschauen was der Fehler sein könnte.

Bei mir war das Problem das ich mich beim Fileshare vertippt habe. Deswegen schaut immer das ihr alles richtig eingetippt habt.

![Bild](../images/Zugriffs%20verweigerung.png)
