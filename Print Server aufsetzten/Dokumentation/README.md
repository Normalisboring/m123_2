<h1><font>Print-Server aufsetzen</font></h1>

---

[TOC]

## Einleitung

Hier erwartet sie eine Anleitung für das aufsetzen von einem Print Server mit dem Betriebssystem von Windows.

Ich starte mit der Annahme, dass sie schon eine Server von Windows installiert haben und jetzt bereit sind mit mir den Server, auf VMware Workstation Pro oder einem anderen Programm aufzusetzen. Ich habe mich für einen Windows Server 2022 entschieden.

## Statische IP-Adresse vergeben

Der erste Schritt besteht darin dem Server eine Statische IP-Adressse zu geben. Da sie gerade versuchen eine Server aufzusetzen, glaube ich daran das sie wissen wie eine Statische IP-Adresse vergeben wird und werde dies daher nicht erklären.

## Voreinstellungen

Ihr Server sollte diese Voreinstellungen haben. Danach kann der Server auch schon gestartet werden.

![Bild](../images/Bridged%20hinzufügen.png)

## Webserver hinzufügen

Fügen sie einen Webserver hinzu. Durch das klicken auf Rollen und Features hinzufügen.

![Bild](../images/Webserver%20hinzufügen%20(am%20Anfang).png)

## Druckerserver hinzufügen

Klicken sie mit der rechten Taste und dann auf Drucker hinzufügen.

![Bild](../images/Drucker%20hinzufügen.png)

Als nächstes wird der Drucker gesucht und ausgewählt.

![Bild](../images/Drucker%20suchen.png)

## Drucker beim Client

Geben Sie die IP-Adresse von Ihrem Server ein, dann wird der Drucker beim Client gesehen.

![Bild](../images/Printer%20wird%20gesehen%20bei%20Client.png)
    